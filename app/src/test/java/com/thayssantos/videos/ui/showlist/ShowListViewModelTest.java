package com.thayssantos.videos.ui.showlist;

import com.thayssantos.videos.data.DataRepository;
import com.thayssantos.videos.data.api.Tv4Service;
import com.thayssantos.videos.model.shows.Show;
import com.thayssantos.videos.ui.showslist.ShowsListViewModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.Observer;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class ShowListViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Mock
    Tv4Service tv4Service;
    @Mock
    DataRepository dataRepository;
    private ShowsListViewModel viewModel;
    @Mock
    Observer<List<Show>> observer;
    @Mock
    LifecycleOwner lifecycleOwner;
    Lifecycle lifecycle;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        lifecycle = new LifecycleRegistry(lifecycleOwner);
        viewModel = new ShowsListViewModel();
        viewModel.videos.observeForever(observer);
    }

    @Test
    public void testNull() {
        when(dataRepository.getShows(1)).thenReturn(null);
        assertNotNull(viewModel.videos);
        assertTrue(viewModel.videos.hasObservers());
    }

    @After
    public void tearDown() {
        dataRepository = null;
        viewModel = null;
    }

}
