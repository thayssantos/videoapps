package com.thayssantos.videos.model.shows;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ShowTest {

    private final String name = "Testing Title";
    private final String summary = "Testing summary";
    private final String premiered = "29-01-2019";

    @Mock
    Show show;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.when(show.getName()).thenReturn(name);
        Mockito.when(show.getSummary()).thenReturn(summary);
        Mockito.when(show.getPremiered()).thenReturn(premiered);
    }

    @Test
    public void testShowsTitle(){
        Mockito.when(show.getName()).thenReturn(name);
        Assert.assertEquals("Testing Title",show.getName());
    }

    @Test
    public void testShowsSummary(){
        Mockito.when(show.getSummary()).thenReturn(summary);
        Assert.assertEquals("Testing summary",show.getSummary());
    }

    @Test
    public void testShowsPremiered(){
        Mockito.when(show.getPremiered()).thenReturn(premiered);
        Assert.assertEquals("29-01-2019",show.getPremiered());
    }

    @Test
    public void testShowsTitleIncorrect(){
        Mockito.when(show.getName()).thenReturn(name);
        Assert.assertNotEquals("Title",show.getName());
    }

    @Test
    public void testShowsSummaryIncorrect(){
        Mockito.when(show.getSummary()).thenReturn(summary);
        Assert.assertNotEquals("summary",show.getSummary());
    }

    @Test
    public void testShowsPremieredIncorrect(){
        Mockito.when(show.getPremiered()).thenReturn(premiered);
        Assert.assertNotEquals("29-02-2019",show.getPremiered());
    }

    @After
    public void tearDown() throws Exception {
        show = null;
    }
}
