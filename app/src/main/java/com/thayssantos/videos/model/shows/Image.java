package com.thayssantos.videos.model.shows;

import java.io.Serializable;

public class Image implements Serializable {
	private String original;
	private String medium;

	public void setOriginal(String original){
		this.original = original;
	}

	public String getOriginal(){
		return original;
	}

	public void setMedium(String medium){
		this.medium = medium;
	}

	public String getMedium(){
		return medium;
	}

	@Override
 	public String toString(){
		return 
			"Image{" + 
			"original = '" + original + '\'' + 
			",medium = '" + medium + '\'' + 
			"}";
		}
}
