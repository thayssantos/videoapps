package com.thayssantos.videos.model.shows;

import java.io.Serializable;

public class Externals implements Serializable {
	private int thetvdb;
	private String imdb;
	private int tvrage;

	public void setThetvdb(int thetvdb){
		this.thetvdb = thetvdb;
	}

	public int getThetvdb(){
		return thetvdb;
	}

	public void setImdb(String imdb){
		this.imdb = imdb;
	}

	public String getImdb(){
		return imdb;
	}

	public void setTvrage(int tvrage){
		this.tvrage = tvrage;
	}

	public int getTvrage(){
		return tvrage;
	}

	@Override
 	public String toString(){
		return 
			"Externals{" + 
			"thetvdb = '" + thetvdb + '\'' + 
			",imdb = '" + imdb + '\'' + 
			",tvrage = '" + tvrage + '\'' + 
			"}";
		}
}
