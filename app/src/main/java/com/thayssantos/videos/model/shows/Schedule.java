package com.thayssantos.videos.model.shows;

import java.io.Serializable;
import java.util.List;

public class Schedule implements Serializable {
	private List<String> days;
	private String time;

	public void setDays(List<String> days){
		this.days = days;
	}

	public List<String> getDays(){
		return days;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	@Override
 	public String toString(){
		return 
			"Schedule{" + 
			"days = '" + days + '\'' + 
			",time = '" + time + '\'' + 
			"}";
		}
}