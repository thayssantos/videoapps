package com.thayssantos.videos.model.shows;

import java.io.Serializable;
import java.util.List;

public class Show implements Serializable {
	private String summary;
	private Image image;
	private Links links;
	private String premiered;
	private Rating rating;
	private int runtime;
	private int weight;
	private String language;
	private String type;
	private String url;
	private String officialSite;
	private Network network;
	private Schedule schedule;
	private WebChannel webChannel;
	private List<String> genres;
	private String name;
	private int id;
	private Externals externals;
	private int updated;
	private String status;

	public void setSummary(String summary){
		this.summary = summary;
	}

	public String getSummary(){
		return summary;
	}

	public void setImage(Image image){
		this.image = image;
	}

	public Image getImage(){
		return image;
	}

	public void setLinks(Links links){
		this.links = links;
	}

	public Links getLinks(){
		return links;
	}

	public void setPremiered(String premiered){
		this.premiered = premiered;
	}

	public String getPremiered(){
		return premiered;
	}

	public void setRating(Rating rating){
		this.rating = rating;
	}

	public Rating getRating(){
		return rating;
	}

	public void setRuntime(int runtime){
		this.runtime = runtime;
	}

	public int getRuntime(){
		return runtime;
	}

	public void setWeight(int weight){
		this.weight = weight;
	}

	public int getWeight(){
		return weight;
	}

	public void setLanguage(String language){
		this.language = language;
	}

	public String getLanguage(){
		return language;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setOfficialSite(String officialSite){
		this.officialSite = officialSite;
	}

	public String getOfficialSite(){
		return officialSite;
	}

	public void setNetwork(Network network){
		this.network = network;
	}

	public Network getNetwork(){
		return network;
	}

	public void setSchedule(Schedule schedule){
		this.schedule = schedule;
	}

	public Schedule getSchedule(){
		return schedule;
	}

	public void setWebChannel(WebChannel webChannel){
		this.webChannel = webChannel;
	}

	public WebChannel getWebChannel(){
		return webChannel;
	}

	public void setGenres(List<String> genres){
		this.genres = genres;
	}

	public List<String> getGenres(){
		return genres;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setExternals(Externals externals){
		this.externals = externals;
	}

	public Externals getExternals(){
		return externals;
	}

	public void setUpdated(int updated){
		this.updated = updated;
	}

	public int getUpdated(){
		return updated;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Show{" +
			"summary = '" + summary + '\'' + 
			",image = '" + image + '\'' + 
			",_links = '" + links + '\'' + 
			",premiered = '" + premiered + '\'' + 
			",rating = '" + rating + '\'' + 
			",runtime = '" + runtime + '\'' + 
			",weight = '" + weight + '\'' + 
			",language = '" + language + '\'' + 
			",type = '" + type + '\'' + 
			",url = '" + url + '\'' + 
			",officialSite = '" + officialSite + '\'' + 
			",network = '" + network + '\'' + 
			",schedule = '" + schedule + '\'' + 
			",webChannel = '" + webChannel + '\'' + 
			",genres = '" + genres + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",externals = '" + externals + '\'' + 
			",updated = '" + updated + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}