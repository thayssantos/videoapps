package com.thayssantos.videos.model.shows;

import java.io.Serializable;

public class Previousepisode implements Serializable {
	private String href;

	public void setHref(String href){
		this.href = href;
	}

	public String getHref(){
		return href;
	}

	@Override
 	public String toString(){
		return 
			"Previousepisode{" + 
			"href = '" + href + '\'' + 
			"}";
		}
}
