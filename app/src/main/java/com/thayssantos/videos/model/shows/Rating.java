package com.thayssantos.videos.model.shows;

import java.io.Serializable;

public class Rating implements Serializable {
	private Object average;

	public void setAverage(Object average){
		this.average = average;
	}

	public Object getAverage(){
		return average;
	}

	@Override
 	public String toString(){
		return 
			"Rating{" + 
			"average = '" + average + '\'' + 
			"}";
		}
}
