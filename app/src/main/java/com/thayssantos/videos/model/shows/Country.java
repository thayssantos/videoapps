package com.thayssantos.videos.model.shows;

import java.io.Serializable;

public class Country implements Serializable {
	private String code;
	private String timezone;
	private String name;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setTimezone(String timezone){
		this.timezone = timezone;
	}

	public String getTimezone(){
		return timezone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"Country{" + 
			"code = '" + code + '\'' + 
			",timezone = '" + timezone + '\'' + 
			",name = '" + name + '\'' + 
			"}";
		}
}
