package com.thayssantos.videos.model.shows;

import java.io.Serializable;

public class Links implements Serializable {
	private Self self;
	private Previousepisode previousepisode;

	public void setSelf(Self self){
		this.self = self;
	}

	public Self getSelf(){
		return self;
	}

	public void setPreviousepisode(Previousepisode previousepisode){
		this.previousepisode = previousepisode;
	}

	public Previousepisode getPreviousepisode(){
		return previousepisode;
	}

	@Override
 	public String toString(){
		return 
			"Links{" + 
			"self = '" + self + '\'' + 
			",previousepisode = '" + previousepisode + '\'' + 
			"}";
		}
}
