package com.thayssantos.videos.ui;

import com.thayssantos.videos.model.shows.Show;

public interface FragmentListener {
    void displayShowsList();

    void displayShowDetails(Show show);
}
