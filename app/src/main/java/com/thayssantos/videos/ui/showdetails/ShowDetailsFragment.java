package com.thayssantos.videos.ui.showdetails;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.thayssantos.videos.R;
import com.thayssantos.videos.model.shows.Show;
import com.thayssantos.videos.utils.ImageConvertor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

public class ShowDetailsFragment extends Fragment {
    private static final String SHOW_KEY = "show.key";
    private final String IMG_RESIZE = "300x300";
    private Show show = null;
    private AppCompatImageView image;
    private AppCompatTextView title;
    private AppCompatTextView broadcastTime;
    private AppCompatTextView description;

    public static ShowDetailsFragment newInstance(Show show) {
        ShowDetailsFragment fragment = new ShowDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(SHOW_KEY, show);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            show = (Show) getArguments().getSerializable(SHOW_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.show_details_fragment, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        image = view.findViewById(R.id.show_detail_image);
        title = view.findViewById(R.id.show_detail_title);
        broadcastTime = view.findViewById(R.id.show_detail_broadcast_time);
        description = view.findViewById(R.id.show_detail_description);

        onBind(view.getContext());
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void onBind(Context context) {
        if (show != null) {
            title.setText(show.getName());
            description.setText(Html.fromHtml(show.getSummary(), Html.FROM_HTML_MODE_COMPACT));
            broadcastTime.setText(show.getPremiered());

            Glide.with(context)
                    .load(ImageConvertor.getConvertedImage(show.getImage().getOriginal(), IMG_RESIZE))
                    .centerInside()
                    .into(image);
        }
    }
}
