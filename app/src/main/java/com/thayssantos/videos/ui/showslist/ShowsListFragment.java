package com.thayssantos.videos.ui.showslist;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.thayssantos.videos.R;
import com.thayssantos.videos.model.shows.Show;
import com.thayssantos.videos.ui.FragmentListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ShowsListFragment extends Fragment {

    private FragmentListener fragmentListener = null;
    private ShowsListViewModel mViewModel;
    private RecyclerView showsRecycleView;
    private ProgressBar progressBar;
    private View viewProgress;
    private ShowsResultRecyclerAdapter showsResultRecyclerAdapter;
    private Handler handler = new Handler();
    private Runnable runnableCode;

    public static ShowsListFragment newInstance() {
        return new ShowsListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ShowsListViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.show_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        showsRecycleView = view.findViewById(R.id.shows);
        showsRecycleView.setLayoutManager(new LinearLayoutManager(getContext()));
        showsResultRecyclerAdapter = new ShowsResultRecyclerAdapter(getContext(), this::startShowDetailsScreen);
        showsRecycleView.setAdapter(showsResultRecyclerAdapter);

        progressBar = view.findViewById(R.id.progressBar);
        viewProgress = view.findViewById(R.id.viewProgress);

        mViewModel.isLoading.observe(this, isLoading -> {
            progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
            viewProgress.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        });

        mViewModel.videos.observe(this, videos -> {
            showsResultRecyclerAdapter.addShowsToList(videos);
            handler.postDelayed(runnableCode, 300000);
        });

        setRefreshHandler();

        mViewModel.msg.observe(this, this::showToast);

        mViewModel.getShows(1);
    }

    private void setRefreshHandler() {
        runnableCode = () -> {
            mViewModel.getShows(1);
        };
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private void startShowDetailsScreen(Show show) {
        fragmentListener.displayShowDetails(show);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentListener = (FragmentListener) getActivity();
    }

    @Override
    public void onDetach() {
        fragmentListener = null;
        super.onDetach();
    }
}
