package com.thayssantos.videos.ui;

import com.thayssantos.videos.data.DataRepository;

public class Injection {

    public static DataRepository provideDataRepository() {
        return new DataRepository();
    }

}
