package com.thayssantos.videos.ui.showslist;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.thayssantos.videos.R;
import com.thayssantos.videos.model.shows.Show;
import com.thayssantos.videos.utils.ImageConvertor;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

public class ShowsResultRecyclerAdapter extends
        RecyclerView.Adapter<ShowsResultRecyclerAdapter.ViewHolder> {
    private final String IMG_RESIZE = "150x150";
    private List<Show> shows = new ArrayList<>();
    private final OnItemClickListener listener;
    private Context context;

    public interface OnItemClickListener {
        void onItemClick(Show item);
    }

    public ShowsResultRecyclerAdapter(Context context, OnItemClickListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.show_item, parent, false);

        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(shows.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return shows.size();
    }

    void addShowsToList(List<Show> showsList) {
        shows.clear();
        shows.addAll(showsList);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View itemView;
        private AppCompatImageView image;
        private AppCompatTextView title;
        private AppCompatTextView broadcastTime;
        private AppCompatTextView description;

        private ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            image = itemView.findViewById(R.id.show_image);
            title = itemView.findViewById(R.id.show_title);
            broadcastTime = itemView.findViewById(R.id.show_broadcast_time);
            description = itemView.findViewById(R.id.show_description);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        void bind(final Show show, final OnItemClickListener listener) {
            title.setText(show.getName());
            description.setText(Html.fromHtml(show.getSummary(), Html.FROM_HTML_MODE_COMPACT));
            broadcastTime.setText(show.getPremiered());

            Glide.with(context)
                    .load(ImageConvertor.getConvertedImage(show.getImage().getOriginal(), IMG_RESIZE))
                    .centerCrop()
                    .into(image);

            itemView.setOnClickListener(v -> listener.onItemClick(show));
        }
    }
}