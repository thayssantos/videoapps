package com.thayssantos.videos.ui.showslist;

import com.thayssantos.videos.data.DataRepository;
import com.thayssantos.videos.model.shows.Show;
import com.thayssantos.videos.ui.Injection;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ShowsListViewModel extends ViewModel {
    private MutableLiveData<List<Show>> _videos;
    private MutableLiveData<String> _msg;
    private MutableLiveData<Boolean> _isLoading;
    public LiveData<List<Show>> videos;
    public LiveData<String> msg;
    public LiveData<Boolean> isLoading;
    private CompositeDisposable compositeDisposable;

    private DataRepository dataRepository;

    public ShowsListViewModel() {
        dataRepository = Injection.provideDataRepository();
        _videos = new MutableLiveData<>();
        videos = _videos;

        _msg = new MutableLiveData<>();
        msg = _msg;

        _isLoading = new MutableLiveData<>(false);
        isLoading = _isLoading;
        compositeDisposable = new CompositeDisposable();
    }

    public void getShows(int page) {
        _isLoading.setValue(true);
        compositeDisposable.add(dataRepository.getShows(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(createObserver()));
    }

    private DisposableObserver<List<Show>> createObserver() {
        return new DisposableObserver<List<Show>>() {
            @Override
            public void onNext(List<Show> showDetails) {
                _videos.postValue(showDetails);
            }

            @Override
            public void onError(Throwable e) {
                _msg.postValue("An error occurred while fetching data.");
                _isLoading.setValue(false);
            }

            @Override
            public void onComplete() {
                _isLoading.setValue(false);
            }
        };
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
