package com.thayssantos.videos.data.api;

import com.thayssantos.videos.model.shows.Show;
import com.thayssantos.videos.utils.Constants;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.thayssantos.videos.utils.Constants.GET_SHOWS_ENDPOINT;

public interface Tv4Service {

    @GET(GET_SHOWS_ENDPOINT)
    Observable<List<Show>> getShows(
            @Query(Constants.QUERY_PARAM_PAGE) int page);
}
