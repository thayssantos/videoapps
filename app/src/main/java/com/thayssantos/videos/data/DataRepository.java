package com.thayssantos.videos.data;


import com.thayssantos.videos.data.api.ServiceFactory;
import com.thayssantos.videos.data.api.Tv4Service;
import com.thayssantos.videos.model.shows.Show;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Retrofit;

public class DataRepository {
    private final Tv4Service tv4Service;
    private Retrofit retrofitClient;

    public DataRepository() {
        retrofitClient = ServiceFactory.getInstance().createRetrofit();
        tv4Service = retrofitClient.create(Tv4Service.class);
    }

    public Observable<List<Show>> getShows(int page) {
        return tv4Service.getShows(page);
    }
}
