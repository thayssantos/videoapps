package com.thayssantos.videos;

import android.os.Bundle;

import com.thayssantos.videos.model.shows.Show;
import com.thayssantos.videos.ui.FragmentListener;
import com.thayssantos.videos.ui.showdetails.ShowDetailsFragment;
import com.thayssantos.videos.ui.showslist.ShowsListFragment;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements FragmentListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            displayShowsList();
        }
    }

    @Override
    public void displayShowsList() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, ShowsListFragment.newInstance())
                .commit();
    }

    @Override
    public void displayShowDetails(Show show) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, ShowDetailsFragment.newInstance(show))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
