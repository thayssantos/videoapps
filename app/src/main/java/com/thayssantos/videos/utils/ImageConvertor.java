package com.thayssantos.videos.utils;

import android.net.Uri;

public class ImageConvertor {
    private static final String CONVERTOR_BASE = "imageproxy.b17g.services";

    public static String getConvertedImage(String imageUrl, String resize) {
        Uri.Builder url = new Uri.Builder();
        url.scheme("https")
            .authority(CONVERTOR_BASE)
            .appendPath("convert")
            .appendQueryParameter("source", imageUrl)
            .appendQueryParameter("resize", resize);

        return url.toString();
    }
}
