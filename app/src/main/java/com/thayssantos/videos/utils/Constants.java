package com.thayssantos.videos.utils;

public class Constants {
    public static final String GET_SHOWS_ENDPOINT = "shows";

    public static final String QUERY_PARAM_PAGE = "page";
}
